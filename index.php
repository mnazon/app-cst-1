<?php

use App\Kernel;

const ROOT_DIR = __DIR__.'/';

require_once ROOT_DIR.'vendor/autoload.php';

$kernel = new Kernel();
$kernel->initApp();