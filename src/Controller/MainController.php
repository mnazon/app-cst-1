<?php

namespace App\Controller;

use App\core\Controller;

class MainController extends Controller
{
    public function index()
    {
        $this->view->generate('main/index_view.php', ['action' => self::class.'::index']);
    }
}
