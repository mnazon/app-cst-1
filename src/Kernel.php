<?php

namespace App;

class Kernel
{
    private $debug;

    public function __construct()
    {
        $this->debug = false;
    }

    public function initApp()
    {
        $this->configureApp();
        $this->sessionStart();
        $this->routeStart();
    }

    private function configureApp()
    {
        $appConfig = require ROOT_DIR.'/config/app.php';
        if (isset($appConfig['debug']) && $this->debug = $appConfig['debug']) {
            error_reporting( E_ALL ^ E_NOTICE ^ E_WARNING);
            ini_set('display_errors', 1);
        }
    }

    private function sessionStart()
    {
        session_start();
    }

    private function routeStart()
    {
        \App\core\Route::init()->start();
    }
}
