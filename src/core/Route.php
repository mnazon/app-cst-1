<?php

namespace App\core;

final class Route
{
    private $defaultControllerName = 'Main';
    private $defaultActionName = 'index';

    public static function init()
    {
        return new self();
    }

    public function start()
    {
        $requestUri = explode('?', $_SERVER['REQUEST_URI']);
        $routes = explode('/', $requestUri[0]);
        $params = isset($requestUri[1]) ? $requestUri[1] : [];

        $controllerName = !empty($routes[1]) ? $routes[1] : $this->defaultControllerName;
        $action = !empty($routes[2]) ? $routes[2] : $this->defaultActionName;

        try {
            $this->callActionControllerBy($controllerName, $action, $params);
        } catch (\Exception $e) {
            throw new \HttpRequestException('Page not found');
        }
    }

    /**
     * @param string $controllerName
     * @param string $action
     * @param array  $params
     * @throws \HttpRequestException
     * @throws \ReflectionException
     */
    private function callActionControllerBy($controllerName, $action, $params)
    {
        $controller = $this->getControllerBy($controllerName);
        $reflectionMethod = new \ReflectionMethod($controller, $action);
        if (!$reflectionMethod) {
            throw new \HttpRequestException();
        }

        if ($reflectionMethod->getParameters()) {
            $controller->$action($params);
        } else {
            $controller->$action();
        }
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    private function getControllerBy($name)
    {
        $controller = "App\\Controller\\".ucfirst($name).'Controller';
        if (!class_exists($controller)) {
            throw new \HttpRequestException();
        }

        return new $controller();
    }
}
