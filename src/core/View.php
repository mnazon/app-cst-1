<?php

namespace App\core;

final class View
{
    public function generate($contentView = null, $data = [], $baseView = 'base_view.php')
    {
        if (is_array($data)) {
            extract($data);
        }

        include ROOT_DIR.'templates/'.$baseView;
    }
}
